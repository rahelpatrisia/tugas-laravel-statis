<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Genre;

class CastController extends Controller
{
   public function create(){
    return view ('Cast.create');
   }

   public function store (Request $request)
   {
    $request-> validate([
        'nama' => 'required',
        'deskripsi' => 'required', 
    ]);

    $kategori=new Cast;
    $kategori->nama=$request->nama;
    $kategori->deskripsi = $request->deskripsi;
    $kategori->save();

    return redirect ('/cast');
   }

   public function index()
   {
    $kategori = Genre::all();
    // dd ($kategori);
    return view ('Cast.index', compact('kategori') );
   }

   public function show($cast_id){
    $kategori= Genre::find($id);
    return view ('Cast.show', compact('kategori'));
   }
   public function edit ($cast_id)
   {
    $kategori = Cast::where('id', $cast_id)-> first();
    return view ('Cast.edit', compact('kategori'));
   }

   public function update (Request $request, $cast_id)
   {
    $request -> validate([
        'nama'=> 'required',
        'deskripsi'=> 'required',
    ]);
    $kategori = Cast :: find($cast_id);

    $kategori -> nama = $request ['nama'];
    $kategori -> deskripsi = $request ['deskripsi'];
    $kategori -> save();
    return redirect ('/cast');


   }

   public function destroy ($cast_id)
   {
    $kategori = Cast::find($cast_id);
    $kategori->delete();
    return redirect('/cast');
   }
}

