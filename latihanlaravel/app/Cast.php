<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = 'kategori';
    protected $fillable = ['nama', 'deskripsi'];
}
