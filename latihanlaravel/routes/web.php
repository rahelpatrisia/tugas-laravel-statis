<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// route("url", "controller")

Route::get('/', "AuthController@Home");
Route::get('/Register', "IndexController@bio");
Route::post('/welcom', "IndexController@kirim");
Route::get('/data-table', function(){
    return view('table.data-table');
}
);

Route::group(['middleware' => ['auth']], function () {

// Create
Route::get('/cast/create', 'CastController@create'); // menuju form
Route::post('/cast', 'CastController@store');  // menyimpan database nya

// Read
Route::get('/cast', 'CastController@index'); // list kategori
Route::get('/cast/{cast_id}', 'CastController@show'); // list

// Update data
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // route menuju form edit
Route::put('/cast/{cast_id}', 'CastController@update'); // route untuk mengupdate data berdasarkan database

// delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //route untuk hapus database nya

// Update Profil
Route::resource('profil', 'ProfilController')->only([
    'index', 'update'
]);
});

// CRUD KATEGORI?
Route::resource('berita', 'BeritController');
Auth::routes();




// kategori= genre
