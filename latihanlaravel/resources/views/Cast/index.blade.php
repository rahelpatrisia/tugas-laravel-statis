@extends('layout.master')

@section('judul')
Daftar Pemain
@endsection

@section('content')
<a href="/cast/create" class="btn btn-secondary mb-3"> Tambah Pemain </a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama </th>
        <th scope="col">Deskripsi</th>
        <th scope="col">Action</th>
        <th scope="col">List Film</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($kategori as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item -> nama}}</td>
                <td>{{$item -> deskripsi}}</td>
                <td>
                    <ul>
                        @foreach ($item ->berita as $value)
                        <li>{{$value->judul}}</li>
                        @endforeach      
                    </ul>
                  
                </td>
                <td>
                    <form action="/cast/{{$item ->id}}" method = "POST">
                        <a href="/cast/{{$item ->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$item ->id}}/edit" class="btn btn-danger btn-sm">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value ="Delete">
                    </form>>
                </td>
            </tr>
        @empty
            <h1>Data Tidak Ada</h1>
        @endforelse
    </tbody>
  </table>
  @endsection