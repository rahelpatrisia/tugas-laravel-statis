@extends('layout.master')

@section('judul')
List Pemain
@endsection

@section('content')
<h1> {{$kategori->nama}} </h1>
<p> {{$kategori->deskripsi}} </p>
<div class="card">
    <img src="{{asset('gambar/'.$kategori->berita->thumbnail)}}" class="card-img-top" alt="...">
    <div class="card-body">
      <span class="badge badge-info">{{$kategori->nama}}</span>
      <h2> {{$kategori->berita->judul}}</h2>
      <p class="card-text">{{$kategori->berita->content}}</p>

    </div>
@endsection