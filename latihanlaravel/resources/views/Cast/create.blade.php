@extends('layout.master')

@section('judul')
Tambah Pemain
@endsection

@section('content')
<form action ='/cast' method='POST'>
  @csrf
    <div class="form-group">
      <label>Nama Pemain</label>
      <input type="text" name="nama" class='form-control' >
    </div>
    @error('nama')
    <div class ='alert alert-danger'>{{$message}}</div>
    @enderror
    <div class="form-group">
      <label >Deskripsi</label>
      <textarea name= "deskripsi" class= "form-control"></textarea>
    </div>
    @error('nama')
    <div class ='alert alert-danger'>{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  @endsection 