@extends('layout.master')

@section('judul')
Tambah Film
@endsection

@section('content')

@auth
<a href="/berita/create" class='btn btn-primary my-2'>Tambah</a>  
@endauth


<a href="cast/create" class="btn btn-info btn-sm my-2">Tambah Film</a>
<div class='row'>
@forelse ($berita as $item)
<div class='col-4'>
    <div class="card">
        <img src="{{asset('gambar/'.$item->thumbnail)}}" class="card-img-top" alt="...">
        <div class="card-body">
          <span class="badge badge-info">{{$item->kategori->nama}}</span>
          <h2> {{$item->judul}}</h2>
          <p class="card-text">{{Str::limit($item->content, 30)}}{{$item->content}}</p>
          @auth
          <form action="/berita/{{$item->id}}" method='POST'>
            @csrf
            @method('DELETE')
            <a href="/berita/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            <input type="submit"  value="Delete" class='btn btn-danger btn-sm'>
          </form>
          @endauth
@guest
<a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
@endguest

        </div>
    </div>
    </div>
    
@empty
    <h2> Film Belum Ada</h2>
@endforelse

</div>

  @endsection