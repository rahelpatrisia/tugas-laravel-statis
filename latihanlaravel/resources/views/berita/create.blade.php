@extends('layout.master')

@section('judul')
List Film
@endsection

@section('content')
<form action ='/gambar' method='POST' enctype="multipart/form-data">
  @csrf

    <div class="form-group">
      <label>Judul Film</label>
      <input type="text" name="judul" class='form-control' >
    </div>
    @error('judul')
    <div class ='alert alert-danger'>{{$message}}</div>
    @enderror

    <div class="form-group">
      <label >Content</label>
      <textarea name="content" class= "form-control"></textarea>
    </div>
    @error('content')
    <div class ='alert alert-danger'>{{$message}}</div>
    @enderror
    
    <div class="form-group">
      <label >Genre</label>
      <select name="kategori_id" class="form-control" id="">
        <option value=""> --Pilih Genre-- </option>
        @foreach ($kategori as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
        @endforeach
      </select>
    </div>
    @error('kategori_id')
    <div class ='alert alert-danger'>{{$message}}</div>
    @enderror

    <div class="form-group">
      <label >Poster</label>
      <input type="file" name="thumbnail" class="form-control">
    </div>
    @error('thumbnail')
    <div class ='alert alert-danger'>{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  @endsection 
  