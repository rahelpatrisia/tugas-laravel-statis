
@extends('layout.master')

@section('judul')
Update Profil
@endsection

@section('content')
<form action ='/profil/{{$profile->id}}' method='POST'>
  @csrf
  @method('PUT')
    <div class="form-group">
      <label>Nama User</label>
      <input type="text" value ="{{$profile->user->name}}" class='form-control' disabled>
    </div>
    
    <div class="form-group">
      <label >Email User</label> <br>
      <input type="text" value="{{$profile->user->email}}" class='form-control' disabled>

    </div>
    <div class="form-group">
    <label >Bio</label> <br>
    <textarea name="bio" id="form-control" cols="20" rows="10">{{$profile->bio}}</textarea>
  </div>
  @error('bio')
  <div class ='alert alert-danger'>{{$message}}</div>
  @enderror
    <div class="form-group">
      <label >umur</label> <br>
      <input type="number" name='umur' value="{{$profile->umur}}" class='form-control'>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  @endsection 